package de.moke12g.MoKeCore;

import java.text.SimpleDateFormat;
import java.util.Date;

public class BConsole {
    public static void Write(String Message, boolean line, boolean Time) {
        if (!line && !Time) System.out.print(Message);
        if (line && !Time) System.out.println(Message);
        if (!line && Time)
            System.out.print(new SimpleDateFormat("dd.MM.yyyy HH:mm:ss:ms").format(new Date()) + " " + Message);
        if (line && Time)
            System.out.println(new SimpleDateFormat("dd.MM.yyyy HH:mm:ss:ms").format(new Date()) + " " + Message);
    }
}
