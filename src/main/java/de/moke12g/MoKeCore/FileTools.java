package de.moke12g.MoKeCore;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

public class FileTools {
    static String os = "noscan";

    public static String Pfad(String programmName) {
        String Pfad = System.getProperty("user.home");
        os = System.getProperty("os.name");
        Pfad = Pfad + "/" + programmName + "/";
        return Pfad;
    }

    public static void deleteDir(File path) {
        int i = 0;
        try {
            for (File file : path.listFiles()) {
                if (file != null) {
                    if (file.isDirectory())
                        deleteDir(file);
                    file.delete();
                    i++;
                    BConsole.Write("Lösche \"" + file.getAbsolutePath() + "\"", true, true);
                }
            }
            path.delete();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        BConsole.Write(i + " Datein gelöscht.", true, true);
    }

    public static void copyFile(File in, File out) throws IOException {
        if (out.exists() == false) out.createNewFile();
        FileChannel inChannel = null;
        FileChannel outChannel = null;
        try {
            inChannel = new FileInputStream(in).getChannel();
            outChannel = new FileOutputStream(out).getChannel();
            inChannel.transferTo(0, inChannel.size(), outChannel);
        } catch (IOException e) {
            throw e;
        } finally {
            try {
                if (inChannel != null)
                    inChannel.close();
                if (outChannel != null)
                    outChannel.close();
            } catch (IOException e) {
            }
        }
    }
}