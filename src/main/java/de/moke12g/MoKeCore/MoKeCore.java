package de.moke12g.MoKeCore;

public class MoKeCore {

    public static final String programmVersion = "0.5e";

    public static String version() {
        return programmVersion;
    }
}
