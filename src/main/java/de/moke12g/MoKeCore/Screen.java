package de.moke12g.MoKeCore;

import java.awt.*;

public class Screen {

    public static Dimension getScreenResolution() {
        Toolkit t = Toolkit.getDefaultToolkit();
        return t.getScreenSize();
    }
}
