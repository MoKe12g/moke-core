package de.moke12g.MoKeCore;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class Settings {

    private final String newLine = "#~#new-q.ap-Mo#Ke-le.q-line#~#";
    private final String at = "#~#at-q.ap-Mo#Ke-le.q-at#~#";
    private final String doubleDot = "#~#double-q.ap-Mo#Ke-le.q-dot#~#";

    private File file;
    private HashMap<String, String> hashmap;
    private boolean isEdited;

    public Settings() {
        hashmap = new HashMap<>();
        isEdited = false;
    }

    public Settings(File file) throws IOException {
        this.file = file;
        hashmap = new HashMap<>();
        this.loadSettings();
    }

    public Settings(File file, boolean shouldLoad) throws IOException {
        this.file = file;
        hashmap = new HashMap<>();
        if (shouldLoad) this.loadSettings();
        isEdited = false;
    }

    public Settings(HashMap<String, String> hashmap, File file) {
        this.hashmap = hashmap;
        this.file = file;
        isEdited = false;
    }

    public String getEntry(String settingName) {
        return hashmap.get(settingName.toLowerCase());
    }

    public void setEntry(String settingName, String Text) {
        hashmap.put(settingName.toLowerCase(), Text);
        isEdited = true;
    }

    public void delEntry(String settingName) {
        hashmap.remove(settingName.toLowerCase());
        isEdited = true;
    }

    public void loadSettings() throws IOException {
        if (!file.exists()) file.createNewFile();
        hashmap = new HashMap<>();
        FileReader fr = new FileReader(file.getAbsoluteFile().toString());
        BufferedReader reader = new BufferedReader(fr);
        String line = reader.readLine();
        while (line != null && line.charAt(0) == '@') {
            line = line.replaceFirst("@", ""); //Um das "@" am Anfang zu vernichten
            line = line.replaceAll(at, "@");
            line = line.replaceAll(newLine, "\n");
            String[] a = line.split(":", 2);
            hashmap.put(a[0].replaceAll(doubleDot, ":"), a[1].replaceAll(doubleDot, ":"));
            line = reader.readLine();
        }
        reader.close();
        isEdited = false;
    }

    public void saveSettings() throws IOException {
        FileWriter fw = new FileWriter(file);
        BufferedWriter writer = new BufferedWriter(fw);
        for (Map.Entry<String, String> entry : hashmap.entrySet()) {
            String Key = entry.getKey().toLowerCase();
            Key = Key.replaceAll("\n", newLine);
            Key = Key.replaceAll("@", at);
            Key = Key.replaceAll(":", doubleDot);

            String Value = entry.getValue();
            Value = Value.replaceAll("\n", newLine); // Ersetzt störende Zeichen
            Value = Value.replaceAll("@", at);
            Value = Value.replaceAll(":", doubleDot);

            writer.write("@" + Key + ":" + Value);
            writer.newLine();
        }
        writer.close();
        isEdited = false;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
        isEdited = true;
    }
}
